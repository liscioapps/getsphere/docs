import { defineConfig } from "astro/config";
import starlight from "@astrojs/starlight";

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  site: "https://docs.getsphere.dev",
  integrations: [
    starlight({
      title: "GetSphere Docs",
      customCss: ["./src/tailwind.css"],
      logo: {
        src: "./src/assets/apple-touch-icon.png",
      },
      social: {
        gitlab: "https://gitlab.com/liscioapps/getsphere",
        discord: "https://discord.gg/GVNbqEvj",
      },
      head: [
        {
          tag: 'script',
          attrs: {
            src: 'https://analytics.liscioapps.com/script.js',
            'data-website-id': 'c4e4971e-c05f-43de-b84d-4bc1e6f37102',
            'data-domains': 'docs.getsphere.dev',
            defer: true,
          },
        },
      ],
      sidebar: [
        {
          label: "Getting Started",
          items: [
            {
              label: "Introduction",
              link: "/getting-started/intro/",
            },
            {
              label: "Start with GetSphere.dev",
              link: "/getting-started/start/",
            },
            {
              label: "Integrations",
              link: "/getting-started/integrations/",
            },
            {
              label: "Data Model",
              link: "/getting-started/data/",
            },
          ],
        },
        {
          label: "Guides",
          items: [
            {
              label: "GetSphere Home",
              link: "/guides/home/",
            },
            {
              label: "Contacts",
              link: "/guides/contacts/",
            },
            {
              label: "Organizations",
              link: "/guides/organizations/",
            },
            {
              label: "Activities",
              link: "/guides/activities/",
            },
            {
              label: "Reports",
              link: "/guides/reports/",
            },
            {
              label: "Automations",
              link: "/guides/automations/",
            },
          ],
          autogenerate: {
            directory: "guides",
          },
        },
        {
          label: "Integrations",
          collapsed: true,
          autogenerate: {
            directory: "integrations",
          },
        },
        {
          label: "Technical Docs",
          collapsed: true,
          autogenerate: {
            directory: "technical-docs",
          },
        },
      ],
    }),
    tailwind({
      applyBaseStyles: false,
    }),
  ],
});
