---
title: Reports
---

import { Card, CardGrid, Aside } from '@astrojs/starlight/components';

The Reports page in GetSphere empowers you to create custom widgets, organize them into comprehensive reports, and share valuable insights with your team and community. This page provides a set of default reports and allows you to build and customize your own reports to better understand and showcase your community's growth and engagement. Below is a detailed guide to the features available on the Reports page.

![GetSphere Reports page screenshot](/img/screenshot-reports.png)

## Default Reports

### Contacts
Gain valuable insights into your community's growth and engagement with the Contacts report.

- **Total Contacts**: Track the overall size of your community over time.
- **Active Contacts**: Monitor the number of actively engaged members.
- **Returning Contacts**: Identify the percentage of members who consistently interact with your community.
- **Contact Leaderboard**: Highlight your most engaged and influential community members.

### Product-Community Fit
Measure and benchmark the alignment between your open-source project and your community's needs and expectations.

- **Product-Community Fit Score**: Assess the overall fit between your project and community.
- **Benchmark Comparison**: Compare your project's fit against industry benchmarks and other open-source projects.
- **Key Metrics**: Track important metrics that contribute to a strong product-community fit.

### Activities
Analyze the frequency, evolution, and distribution of activities within your community.

- **Activity Frequency**: Monitor the number of activities over time to identify engagement trends.
- **Activity Evolution**: Visualize how different types of activities change and grow over time.
- **Platform Usage**: Understand which platforms your community members prefer for interaction.

## Custom Reports

### Create Custom Widgets
Build your own widgets to track and visualize specific metrics and insights relevant to your community.

<Aside type="tip">
	Use the widget editor to select data sources, customize visualizations, and set update frequencies.
</Aside>

### Organize Reports
Arrange your custom widgets into organized reports that provide a comprehensive view of your community's health and growth.

- **Add Sections**: Divide your report into logical sections to group related widgets and insights.
- **Customize Layout**: Arrange widgets using drag-and-drop functionality to create visually appealing and easy-to-understand reports.

### Share Reports
Make your reports accessible to your team, stakeholders, and even your wider community.

- **Team Sharing**: Collaborate with your team by sharing reports within the GetSphere platform.
- **Public Sharing**: Generate public links to share specific reports with your community or other stakeholders.
- **Embedding**: Embed reports on your website or blog to showcase your community's success and engagement.

<Aside type="note">
	Regularly review and update your reports to ensure you have the most accurate and up-to-date insights into your community's health and growth.
</Aside>

By leveraging the default reports and creating custom reports on the Reports page, you can gain valuable insights, make data-driven decisions, and showcase the impact of your community-building efforts.